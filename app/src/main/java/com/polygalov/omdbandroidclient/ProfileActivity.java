package com.polygalov.omdbandroidclient;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

public class ProfileActivity extends AppCompatActivity {

    public String plot;
    public String director;
    public String country;
    public String actors;
    public String title;

    public String rating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        ImageView profileImageView = findViewById(R.id.profileImageView);

        final TextView country = findViewById(R.id.country);
        final TextView actors = findViewById(R.id.actors);
        final TextView director = findViewById(R.id.director);
        final TextView plot = findViewById(R.id.plot);
        final TextView rating = findViewById(R.id.rating);
        final TextView movieTitle = findViewById(R.id.movie_title);


        Intent intent = getIntent();
        String image = intent.getStringExtra(MovieAdapter.KEY_IMAGE);
        final String profileUrl = intent.getStringExtra(MovieAdapter.KEY_URL);

        Picasso.with(this)
                .load(image)
                .into(profileImageView);

        String URL_DATA = "http://www.omdbapi.com/?i=" + profileUrl + "&plot=full&apikey=84e8035e";

        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                URL_DATA, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject jsonObject = new JSONObject(response);

                    title = jsonObject.getString("Title");
                    ProfileActivity.this.rating = jsonObject.getString("imdbRating");
                    ProfileActivity.this.country = jsonObject.getString("Country");
                    ProfileActivity.this.director = jsonObject.getString("Director");
                    ProfileActivity.this.actors = jsonObject.getString("Actors");
                    ProfileActivity.this.plot = jsonObject.getString("Plot");

                    movieTitle.setText(title);
                    rating.setText("IMDB Rating is: " + ProfileActivity.this.rating);
                    country.setText(ProfileActivity.this.country);
                    director.setText(ProfileActivity.this.director);
                    actors.setText(ProfileActivity.this.actors);
                    plot.setText(ProfileActivity.this.plot);

                } catch (JSONException e) {

                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(ProfileActivity.this, "Error" + error.toString(), Toast.LENGTH_SHORT).show();

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }


}

